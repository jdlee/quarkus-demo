#!/bin/bash

dropdb quarkus
createdb quarkus
psql quarkus << EOF
create user quarkus with password 'quarkus';
grant all privileges on database quarkus to quarkus ;
EOF
