package com.steeplesoft.okcjug.model

import io.quarkus.runtime.annotations.RegisterForReflection
import org.hibernate.annotations.GenericGenerator
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
@RegisterForReflection
data class Author(@Id
                  @GeneratedValue(generator="system-uuid")
                  @GenericGenerator(name="system-uuid", strategy = "uuid")
                  var id: String? = null,
                  var name: String? = null)