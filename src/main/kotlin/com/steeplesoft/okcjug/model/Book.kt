package com.steeplesoft.okcjug.model

import org.hibernate.annotations.GenericGenerator
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
data class Book(@Id
                @GeneratedValue(generator="system-uuid")
                @GenericGenerator(name="system-uuid", strategy = "uuid")
                var id: String? = null,
                var title : String? = null)
