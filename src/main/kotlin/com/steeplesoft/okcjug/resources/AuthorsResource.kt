package com.steeplesoft.okcjug.resources

import com.steeplesoft.okcjug.model.Author
import com.steeplesoft.okcjug.services.AuthorJpaService
import javax.inject.Inject
import javax.validation.constraints.NotNull
import javax.ws.rs.BadRequestException
import javax.ws.rs.Consumes
import javax.ws.rs.DELETE
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/authors")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
class AuthorsResource(@Inject private var authorService: AuthorJpaService) {
    @GET
    fun getAuthors(): List<Author> {
        return authorService.getAuthors();
    }

    @GET
    @Path("{id}")
    fun getAuthor(id: String): Author? {
        return authorService.findById(id)
    }

    @POST
    @Path("{id}")
    fun updateAuthor(id: String,
                     @NotNull author: Author): Author {
        if (id != author.id) {
            throw BadRequestException("The author passed does not match the author specified (you can not change the Author id")
        }
        return authorService.updateAuthor(author)
    }

    @POST
    fun addAuthor(@NotNull author: Author): Author {
        return authorService.addAuthor(author)
    }

    @DELETE
    @Path("{id}")
    fun deleteAuthor(id: String): Response.Status {
        return if (authorService.delete(id)) Response.Status.OK else Response.Status.NOT_FOUND
    }
}