package com.steeplesoft.okcjug.services

import com.steeplesoft.okcjug.model.Author
import javax.enterprise.context.ApplicationScoped
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.transaction.Transactional
import javax.validation.constraints.NotNull

@ApplicationScoped
class AuthorJpaService(@PersistenceContext val entityManager: EntityManager ) : AuthorService  {

    @Transactional
    override fun getAuthors(): List<Author> {
        return entityManager.createQuery("SELECT a FROM Author a ORDER BY a.name", Author::class.java).resultList
    }

    @Transactional
    override fun findById(id: String): Author? {
        return entityManager.find(Author::class.java, id)
    }

    @Transactional
    override fun addAuthor(author: Author) : Author {
        entityManager.persist(author)
        return author
    }

    @Transactional
    override fun findAuthorByName(name : String) : Author? {
        val result = entityManager.createQuery("SELECT a FROM Author a WHERE a.name = :name", Author::class.java)
                .setParameter("name", name)
                .resultList
        return if (result.size > 0) result[0] else null
    }

    @Transactional
    override fun updateAuthor(author : Author) : Author {
        return entityManager.merge(author)
    }

    @Transactional
    override fun delete(@NotNull id: String) : Boolean {
        val author = findById(id)
        return if (author != null) {
            entityManager.remove(author)
            true
        } else {
            false
        }
    }
}