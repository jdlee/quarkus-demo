package com.steeplesoft.okcjug.services

import com.steeplesoft.okcjug.model.Author
import javax.validation.constraints.NotNull

interface AuthorService {
    fun findById(id: String): Author?
    fun addAuthor(author: Author): Author
    fun findAuthorByName(name: String): Author?
    fun updateAuthor(author: Author): Author
    fun getAuthors(): List<Author>
    fun delete(@NotNull id: String): Boolean
}