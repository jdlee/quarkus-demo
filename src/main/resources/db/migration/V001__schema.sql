create table book(
    id varchar(255) primary key,
    title varchar(255)
);

create table author(
    id varchar(255) primary key,
    name varchar(255)
);

create sequence hibernate_sequence;