package com.steeplesoft.okcjug

import io.quarkus.test.junit.NativeImageTest

@NativeImageTest
open class NativeAuthorResourceIT : AuthorResourceTest()