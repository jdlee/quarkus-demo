package com.steeplesoft.okcjug

import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import org.junit.jupiter.api.Test

@QuarkusTest
class AuthorResourceTest {

    @Test
    fun testHelloEndpoint() {
        given()
                .`when`().get("/authors")
                .then()
                .statusCode(200)
    }
}