package com.steeplesoft.okcjug

import com.steeplesoft.okcjug.model.Author
import com.steeplesoft.okcjug.services.AuthorService
import io.quarkus.test.junit.QuarkusTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test


@QuarkusTest
class AuthorsServiceTest(private val authorService: AuthorService) {
    @Test
    fun findByName() {
        val author = authorService.findAuthorByName("Tolkien, J.R.R.")
        assertThat(author).isNotNull
    }

    @Test
    fun listAll() {
        assertThat(authorService.getAuthors().size).isGreaterThanOrEqualTo(2)
    }

    @Test
    fun addAuthor() {
        val author = Author(name = "Tolstoy, Leo")
        authorService.addAuthor(author)
        assertThat(author.id).isNotNull()
    }

    @Test
    fun deleteAuthor() {
        assertThat(authorService.findById("-2")).isNotNull
        assertThat(authorService.delete("-2")).isTrue()
        assertThat(authorService.findById("-2")).isNull()

    }
}